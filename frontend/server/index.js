const express = require('express');
const fetch = require('node-fetch');
const morgan = require('morgan')
const path = require("path");

const BACKEND_BASE_URL = `http://${process.env.COLORS_BACKEND_ADDRESS || 'localhost:8080'}`;

const app = express();

app.use(morgan('":method :url HTTP/:http-version" from :remote-addr - :status :res[content-length]B in :response-time ms'));

app.use(express.static(path.join(__dirname, "..", "build")));
app.use(express.static("public"));

app.get('/', (req, res) => {
  res.send('I like colors in the frontend.')
});

app.get(/^\/color[/.]*/, (req, res) => {
  var url = BACKEND_BASE_URL + req.path;

  console.log(`Asking the backend ${BACKEND_BASE_URL} for: ${req.path}`)
     
  fetch(url)
    .then(res => res.json())
    .then(data => {
      console.log('Passing through the response from the backend.');
      res.send(data);
    })
    .catch(err => {
      console.log(`Something went wrong. :(\n${err}`);
      res.status(500).send({'code': 500, 'message': 'Something went horribly wrong.'});
    });
});

module.exports = app;
